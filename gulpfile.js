const gulp = require("gulp");
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const autoprefix = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const uglify = require("gulp-uglify");
const concat = require("gulp-concat");
const imgMin = require("gulp-imagemin");
const newer = require("gulp-newer");
const changed = require("gulp-changed");
const htmlmin = require("gulp-htmlmin");
const replace = require("gulp-html-replace");
const del = require("del");


gulp.task('sass', ()=> {
    return gulp.src("src/scss/**/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefix({}))
        .pipe(sourcemaps.write())
        .pipe(concat("style.css"))
        .pipe(gulp.dest("src/css"))
        .pipe(browserSync.stream());
});

gulp.task("miniJS", ()=> {
    return gulp.src("src/js/*.js")
        .pipe(concat("app.js"))
        .pipe(gulp.dest("src/app"))
        .pipe(browserSync.stream());
});

gulp.task('serve', ()=> {
    browserSync.init({
        server: './src'
    });

    gulp.watch("src/scss/*.scss", gulp.series(gulp.parallel("sass")));
    gulp.watch("src/js/*.js", gulp.parallel("miniJS"));
    gulp.watch("src/*.html").on("change", browserSync.reload);

});

gulp.task("css", ()=>{
    return gulp.src("src/css/style.css")
        .pipe(cleanCSS())
        .pipe(gulp.dest("dist/css"));
});

gulp.task("uglify", () => {
    const polyfill = 'node_modules/babel-polyfill/dist/polyfill.js'
    return gulp.src([polyfill, "src/js/*.js"])
        .pipe(concat("script.js"))
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest("dist/js"));
});

gulp.task("img", ()=> {
    return gulp.src("src/img/*")
        .pipe(newer('dist/img'))
        .pipe(imgMin())
        .pipe(gulp.dest("dist/img"));
})

gulp.task("html", ()=> {
    return gulp.src("src/*.html")
        .pipe(replace({
            "css": 'css/style.css',
            'js': 'js/script.js'
        }))
        .pipe(htmlmin({
            sortAttributes: true,
            sortClassName: true,
            collapseWhitespace: true

        }))
        .pipe(gulp.dest("dist"));
})
gulp.task("clean", ()=> {
    return del(['dist/css', 'dist/js', 'dist/index.html']);
});

gulp.task("build", gulp.series('clean', 'html', 'css', 'uglify', 'img'))

gulp.task('default', gulp.series( 'serve', 'sass'));