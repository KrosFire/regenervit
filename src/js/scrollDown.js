$(document).ready(() => {
  const links = $("a[href*='#']")

  $(links).click(function(e){
    e.preventDefault()
    const target = this.hash
    $('html, body').animate({
      scrollTop: ($(target).offset().top )
    },500);
  })

  $('.dot')[0].focus()
  $($('.navlink')[0]).addClass('set')

  $(document).scroll(() => {
    const top = $(document).scrollTop()
    const bottom = top + $(document).height()

    $(`.dots ul li`).removeClass('set')
    $('.navlink').removeClass('set')

    for(let s of $('section')){
      const elTop = $(s).offset().top
      const elBottom = elTop + $(s).outerHeight()

      if(elBottom > top && elTop < bottom ){
        $(`a.dot[href="#${s.id}"]`).focus()
        $($(`a.dot[href="#${s.id}"]`)[0].parentNode).addClass('set')

        $(`.option a.navlink[href="#${s.id}"]`).focus()
        $(`.option a.navlink[href="#${s.id}"]`).addClass('set')
        break
      }
      
    }
  })
})