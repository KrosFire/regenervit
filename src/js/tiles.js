$(document).ready(()=>{
  const tiles = $('.tile')

  const changeArrow = (ob, up) => {
    if(up){
      $(ob).html(`<p> zwiń <i class="material-icons switch-arrow">
      keyboard_arrow_up
      </i></p></span>`)
    }else{
      $(ob).html(`<p> rozwiń <i class="material-icons switch-arrow">
      keyboard_arrow_down
      </i></p></span>`)
    }
  }

  const hide = (t, ob) => {
    changeArrow(t.children[2],false)
    if(ob == undefined){
      ob = t
    }
    $(ob.children[1]).css('max-height','0')
    $(ob.children[1]).addClass('hidden')

    $(ob).removeClass('expand')
    $(ob).css('height', '240px')

  }
  tiles.click(function(e) {
    const contentHeight = $(this.children[1].children)[0].scrollHeight
    const containerHeight = $(this)[0].scrollHeight

    if(!$(this.children[1]).hasClass('hidden')){
      hide(this)
      return 0;
    }else{
      $(this.children[1]).css('max-height',contentHeight + contentHeight)
      $(this.children[1]).removeClass('hidden')

      $(this).addClass('expand')
      $(this).css( 'height',`${contentHeight + containerHeight - 60 }px`)

      changeArrow(this.children[2],true)
    }

    for(let t of tiles){
      if(!$(t.children[1]).hasClass('hidden') && t !== this){
        hide(this, t)
      }
    }
    
  })
})