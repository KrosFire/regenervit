$(document).ready(() => {
  $('.menu button').click(() => {
    if($('.menu ul').hasClass('slide')){
      $('.menu ul').removeClass('slide')
    }else{
      $('.menu ul').addClass('slide')
    }
  })
})
$(document).ready(() => {
  const links = $("a[href*='#']")

  $(links).click(function(e){
    e.preventDefault()
    const target = this.hash
    $('html, body').animate({
      scrollTop: ($(target).offset().top )
    },500);
  })

  $('.dot')[0].focus()
  $($('.navlink')[0]).addClass('set')

  $(document).scroll(() => {
    const top = $(document).scrollTop()
    const bottom = top + $(document).height()

    $(`.dots ul li`).removeClass('set')
    $('.navlink').removeClass('set')

    for(let s of $('section')){
      const elTop = $(s).offset().top
      const elBottom = elTop + $(s).outerHeight()

      if(elBottom > top && elTop < bottom ){
        $(`a.dot[href="#${s.id}"]`).focus()
        $($(`a.dot[href="#${s.id}"]`)[0].parentNode).addClass('set')

        $(`.option a.navlink[href="#${s.id}"]`).focus()
        $(`.option a.navlink[href="#${s.id}"]`).addClass('set')
        break
      }
      
    }
  })
})
$(document).ready(()=>{
  const tiles = $('.tile')

  const changeArrow = (ob, up) => {
    if(up){
      $(ob).html(`<p> zwiń <i class="material-icons switch-arrow">
      keyboard_arrow_up
      </i></p></span>`)
    }else{
      $(ob).html(`<p> rozwiń <i class="material-icons switch-arrow">
      keyboard_arrow_down
      </i></p></span>`)
    }
  }

  const hide = (t, ob) => {
    changeArrow(t.children[2],false)
    if(ob == undefined){
      ob = t
    }
    $(ob.children[1]).css('max-height','0')
    $(ob.children[1]).addClass('hidden')

    $(ob).removeClass('expand')
    $(ob).css('height', '240px')

  }
  tiles.click(function(e) {
    const contentHeight = $(this.children[1].children)[0].scrollHeight
    const containerHeight = $(this)[0].scrollHeight

    if(!$(this.children[1]).hasClass('hidden')){
      hide(this)
      return 0;
    }else{
      $(this.children[1]).css('max-height',contentHeight + contentHeight)
      $(this.children[1]).removeClass('hidden')

      $(this).addClass('expand')
      $(this).css( 'height',`${contentHeight + containerHeight - 60 }px`)

      changeArrow(this.children[2],true)
    }

    for(let t of tiles){
      if(!$(t.children[1]).hasClass('hidden') && t !== this){
        hide(this, t)
      }
    }
    
  })
})